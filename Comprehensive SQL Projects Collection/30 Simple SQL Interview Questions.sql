/****************************************/
/*   30 Simple SQL Interview Questions    */
/****************************************/

/* 1 Delete the tables Employee, Department, and Company. */
DROP TABLE IF EXISTS Employee;
DROP TABLE IF EXISTS Department;
DROP TABLE IF EXISTS Company;

/*
2.Create tables:
Employee with attributes (id, name, city, department, salary)
Department with attributes (id, name)
Company with attributes (id, name, revenue)
*/
CREATE TABLE department(
	id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL
);

CREATE TABLE company(
	id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    revenue INT
);

CREATE TABLE Employee(
	id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(150) NOT NULL,
    city VARCHAR(150) NOT NULL,
    department_id INT NOT NULL,
    salary INT NOT NULL,
    FOREIGN KEY (department_id) REFERENCES department(id)
);


/*
3.Insert records into the employee table.
(1, 'David', 'London', 'IT', 80000),
(2, 'Emily', 'London', 'IT', 70000),
(3, 'Peter', 'Paris', 'IT', 60000),
(4, 'Ava', 'Paris', 'IT', 50000),
(5, 'Penny', 'London', 'Management', 110000),
(6, 'Jim', 'London', 'Management', 90000),
(7, 'Amy', 'Rome', 'Support', 30000),
(8, 'Cloe', 'London', 'IT', 110000);
*/
INSERT INTO employee (name,city,department_id,salary)
VALUES
('David', 'London', 3, 80000),
('Emily', 'London', 3, 70000),
('Peter', 'Paris', 3, 60000),
('Ava', 'Paris', 3, 50000),
('Penny', 'London', 2, 110000),
('Jim', 'London', 2, 90000),
('Amy', 'Rome', 4, 30000),
('Cloe', 'London', 3, 110000);
/*

4.Add entries to the Department table.
(1, 'IT'),
(2, 'Management'),
(3, 'IT'),
(4, 'Support');
*/
INSERT INTO department(name)
VALUES
('IT'),
('Management'),
('IT'),
('Support');

/*
5.Insert new records into the Company table.
(1, 'IBM', 2000000),
(2, 'GOOGLE', 9000000),
(3, 'Apple', 10000000);
*/
INSERT INTO company(name,revenue)
VALUES
('IBM', 2000000),
('GOOGLE', 9000000),
('Apple', 10000000);


/*
6.Retrieve all records from the Department table.
*/
SELECT * FROM department;

/*
7.Update the name of the department with ID 1 to 'Management'.
*/
UPDATE department
SET name = 'Management'
WHERE id = 1;

/*
8.Remove employees whose salary exceeds $100,000.
*/
DELETE FROM employee
WHERE salary > 100000;

/*
9.Retrieve the name and city of each employee.
*/
SELECT name FROM company;

/*
10.Remove employees whose salary exceeds $100,000.
*/
SELECT name, city
FROM employee;

/*
11.Retrieve all companies with revenue exceeding $5,000,000.
*/
SELECT * FROM company
WHERE revenue > 5000000;

/*
12.Retrieve all companies with revenue less than $5,000,000.
*/
SELECT * FROM company
WHERE revenue < 5000000;

/*
13.Retrieve all companies with revenue not exceeding $5,000,000, without using the '<' operator.
*/
SELECT * FROM company
ORDER BY revenue
LIMIT 1;

/*version 2*/
SELECT * FROM company
WHERE NOT revenue >= 5000000;

/*
14.Retrieve all employees with a salary greater than $50,000 and less than $70,000.
*/
SELECT * FROM employee
WHERE salary BETWEEN 50000 AND 70000;

/*
15.Retrieve all employees with salaries greater than $50,000 and less than $70,000, without using the BETWEEN operator.
*/
SELECT * FROM employee
WHERE salary >= 50000 AND salary <= 70000;

/*
16.Retrieve all employees with a salary of $80,000.
*/
SELECT * FROM employee
WHERE salary = 80000;

/*
17.Retrieve all employees whose salary is different from $80,000.
*/
SELECT * FROM employee
WHERE salary <> 80000;

/*
18.Retrieve the names of employees with salaries exceeding $70,000, along with those who work in the 'IT' department.
*/
SELECT name FROM employee
WHERE salary > 70000
OR department_id IN (
	SELECT id FROM department
    WHERE name = 'IT'
);

/*
19.Retrieve all employees who are based in cities beginning with 'L'.
*/
SELECT * FROM employee
WHERE city LIKE 'L%';

/*
20.Retrieve the names of all employees who work in a city that starts with 'L' or ends with 's'.
*/
SELECT * FROM employee
WHERE city LIKE 'L%' OR city LIKE '%s';

/*
21.Retrieve the names of all employees who work in a city that contains 'o' somewhere in the middle.
*/
SELECT * FROM employee
WHERE city LIKE '%o%';

/*
22.Retrieve the names of all departments, ensuring each name appears only once.
*/
SELECT DISTINCT name FROM department;

/*
22.Retrieve the names of all employees along with the IDs of the departments they work in without using JOIN.
*/
SELECT emp.name,dep.id,dep.name
FROM employee emp, department dep
WHERE emp.department_id = dep.id
ORDER BY emp.name, dep.id;

/*
23.Retrieve the names of all employees along with the IDs of the departments they work in using a JOIN.
*/
SELECT emp.name,dep.id,dep.name
FROM employee emp
JOIN department dep
ON emp.department_id = dep.id
ORDER BY emp.name, dep.id;

/*
24.Retrieve the names of all companies along with the names of all departments (Note: This query may seem unusual as there is typically no direct relationship between companies and departments).
*/
SELECT com.name,dep.name
FROM company com, department dep
ORDER BY com.name;

/*
25.Retrieve the names of all companies along with their departments, excluding the 'Support' department.
*/
SELECT com.name,dep.name
FROM company com, department dep
WHERE dep.name NOT LIKE 'Support'
ORDER BY com.name;

/*
26.Retrieve the names of employees along with the names of departments in which they do not work.
*/
SELECT emp.name, dep.name
FROM employee emp, department dep
WHERE emp.department_id <> dep.id;

/*
27.Retrieve the name of each company along with the names of all other companies. The result should look like:
GOOGLE Apple
GOOGLE IBM
Apple IBM
...
*/
SELECT com1.name, com2.name
FROM company com1, company com2
WHERE com1.name <> com2.name
ORDER BY com1.name,com2.name;


/*
28.Retrieve the names of employees whose salary is less than 80,000 without using NOT and < (PostgreSQL only. MySQL does not support EXCEPT).
*/
SELECT e1.name FROM employee e1
EXCEPT
SELECT e2.name FROM employee e2 WHERE e2.salary >= 80000;


/*
29.Select the names of all companies and rename the column to 'Company'.
*/
SELECT name AS Company
FROM company;

/*
30.Retrieve all employees working in the same department as Peter.
*/
SELECT * FROM employee
WHERE department_id IN(
	SELECT department_id FROM employee
    WHERE name LIKE 'Peter'
)
AND name NOT LIKE 'Peter';